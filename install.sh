#!/bin/bash
OMD=$1
source lib/replace.sh
echo "Installing xinetd & check_mk"
apt-get update && apt-get install -y xinetd
#### CHECK_MK ####
#dpkg -i lib/check-mk-agent_1.2.0p3-2_all.deb
#dpkg -i lib/check-mk-agent-logwatch_1.2.0p3-2_all.deb
dpkg -i agent/check-mk-agent_1.2.6-1_all.deb
replace "__CHECKMK__" "$OMD" lib/xinetd.check_mk
cp lib/xinetd.check_mk /etc/xinetd.d/check_mk
service xinetd restart
test -e /usr/bin/rkhunter && echo "XINETD_ALLOWED_SVC=/etc/xinetd.d/check_mk" >> /etc/rkhunter.conf
